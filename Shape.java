public interface Shape {
    void circle();
}
class Areaofcircle implements Shape
{
    double area;
    public void circle(double r)
    {
        area=(22*r*r)/7;
    }
    public static void main(String args[])
    {
        Areaofcircle x=new Areaofcircle();
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the radius:");
        double rad=sc.nextDouble();
        x.circle(rad);
        System.out.println("Area of circle is: "+ x.area);
    }
}
